<?php
namespace Korireed\DgafClan\Services;

define('BUNGIE_URL','https://www.bungie.net');

class BungieApi
{

    private $cookie_file = null;
    private $baseApiUrl = 'https://www.bungie.net/Platform';
    private $key;
    private $platform;

    public function __construct()
    {
        $this->cookie_file = __DIR__ . '/../../tmp/cookie.txt';
        $this->key = $_SERVER['BUNGIE_API_KEY'];
        $this->platform = $_SERVER['BUNGIE_PLATFORM'];
    }

    public function getGroup($id) {
        $url = '/Group/' . $id . '/';
        return $this->request($url, null);
    }

    public function getGroupMembers($gId, $pNum) {
        $url = '/Group/' . $gId . '/Members/?lc=en&fmt=true&currentPage=' . $pNum . '&platformtype=' . $this->platform;
        return $this->request($url, null);
    }

    public function getGroupAdmins($gId) {
        $url = '/Group/' . $gId . '/Admins/';
        return $this->request($url, null);
    }

    public function getXur()
    {
        $url = '/Destiny/Advisors/Xur/';

        return $this->request($url);
    }

    public function getAdvisors()
    {
        $url = '/Destiny/Advisors/';

        return $this->request($url,null);
    }

    public function getAccount($gamertag)
    {
        $url = '/Destiny/' . $this->platform . '/Account/' . $this->getMembershipIdByGamertag($gamertag)['Response'] . '/';

        return $this->request($url);
    }

    public function getTriumphs($gamertag)
    {
        $url = '/Destiny/' . $this->platform . '/Account/' . $this->getMembershipIdByGamertag($gamertag)['Response'] . '/Triumphs/';

        return $this->request($url);
    }

    public function getHistoryByCharacter($gamertag, $c)
    {
        $charIds = array();
        foreach ($this->getAccount($gamertag)['Response']['data']['characters'] as $character) {
            array_push($charIds, $character['characterBase']['characterId']);
        }
        $url = '/Destiny/Stats/ActivityHistory/' . $this->platform . '/' . $this->getMembershipIdByGamertag($gamertag)['Response'] . '/' . $charIds[$c] . '/?page=0&count=1&definitions=true&mode=4';

        return $this->request($url);
    }

    public function getManifest($type, $hash)
    {
        $url = '/Destiny/Manifest/' . $type . '/' . $hash . '/';

        return $this->request($url);
    }

    public function getMembershipIdByGamertag($gamertag)
    {
        $url = '/Destiny/' . $this->platform . '/Stats/GetMembershipIdByDisplayName/' . rawurlencode($gamertag) . '/';

        return $this->request($url);
    }

    public function request($endpoint, $query)
    {
        $this->do_webauth('xbox','coryjreid@outlook.com','TICKbactoMen');

        $cookies = $this->parseCookieFile($this->cookie_file);
        $bungieCookies = isset($cookies['www.bungie.net']) ? $cookies['www.bungie.net'] : array();

        $url = $this->baseApiUrl . $endpoint;
        if(isset($query)) { $url = $query; }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_file);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-API-Key: ' . $this->key,
            'X-Csrf: ' . (isset($bungieCookies['bungled']) ? $bungieCookies['bungled']['value'] : '')
        ));

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    private function parseCookieFile($file) {
        $cookies = array();
        if (file_exists($file)) {
            $lines = file($file);
            foreach($lines as $line) {
                if (substr_count($line, "\t") == 6) {
                    $tokens = explode("\t", $line);
                    $tokens = array_map('trim', $tokens);

                    $domain = preg_replace('/#[^_]+_/i', '', $tokens[0]);
                    $flag = $tokens[1] == 'TRUE';
                    $path = $tokens[2];
                    $secure = $tokens[3] == 'TRUE';
                    $expiration = $tokens[4];
                    $name = $tokens[5];
                    $value = $tokens[6];
                    if (!isset($cookies[$domain])) $cookies[$domain] = array();
                    $cookies[$domain][$name] = array(
                        'flag' => $flag,
                        'path' => $path,
                        'secure' => $secure,
                        'expiration' => $expiration,
                        'value' => $value
                    );
                }
            }
        }
        return $cookies;
    }

    public function do_webauth($method, $username, $password) {
        $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1";

        $methods = array(
            'psn' => 'Psnid',
            'xbox' => 'Xuid'
        );
        $dest = 'Wlid'; if (isset($methods[$method])) $dest = $methods[$method];
        $url = BUNGIE_URL.'/en/User/SignIn/'.$dest;

        $default_options = array(
            CURLOPT_USERAGENT => $user_agent,
            CURLOPT_COOKIEJAR => $this->cookie_file,
            CURLOPT_COOKIEFILE => $this->cookie_file,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => false
        );

        // Get Third Party Authorization URL
        $ch = curl_init();
        curl_setopt_array($ch, $default_options);
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
        ));
        $Curl_Timeout =10 ;

        curl_exec($ch);
        $redirect_url = curl_getinfo($ch)['redirect_url'];
        curl_close($ch);
        // Bungie Cookies are still valid
        if (!$redirect_url) return true;

        // Try to authenticate with Third Party
        $ch = curl_init();
        curl_setopt_array($ch, $default_options);
        curl_setopt_array($ch, array(
            CURLOPT_URL => $redirect_url,
        ));
        $auth_result = curl_exec($ch);
        $auth_info = curl_getinfo($ch);
        $auth_url = $auth_info['redirect_url'];
        // Normally authentication will produce a 302 Redirect, but Xbox is special...
        if ($auth_info['http_code'] == 200) $auth_url = $auth_info['url'];

        curl_close($ch);

        // No valid cookies
        if (strpos($auth_url, $url.'?code') !== 0) {
            $result = false;
            switch($method) {
                case 'psn':
                    $login_url = 'https://auth.api.sonyentertainmentnetwork.com/login.do';

                    // Login to PSN
                    $ch = curl_init();
                    curl_setopt_array($ch, $default_options);
                    curl_setopt_array($ch, array(
                        CURLOPT_URL => $login_url,
                        CURLOPT_POST => 3,
                        CURLOPT_POSTFIELDS => http_build_query(array(
                            'j_username' => $username,
                            'j_password' => $password,
                            'rememberSignIn' => 1 // Remember signin
                        )),
                    ));
                    curl_exec($ch);
                    $redirect_url = curl_getinfo($ch)['redirect_url'];
                    curl_close($ch);

                    if (strpos($redirect_url, 'authentication_error') !== false) return false;

                    // Authenticate with Bungie
                    $ch = curl_init();
                    curl_setopt_array($ch, $default_options);
                    curl_setopt_array($ch, array(
                        CURLOPT_URL => $redirect_url,
                        CURLOPT_FOLLOWLOCATION => true
                    ));
                    curl_exec($ch);
                    $result = curl_getinfo($ch);
                    curl_close($ch);
                    break;
                case 'xbox':
                    $login_url = 'https://login.live.com/ppsecure/post.srf?'.substr($redirect_url, strpos($redirect_url, '?')+1);
                    preg_match('/id\="i0327" value\="(.*?)"\//', $auth_result, $ppft);

                    if (count($ppft) == 2) {
                        $ch = curl_init();
                        curl_setopt_array($ch, $default_options);
                        curl_setopt_array($ch, array(
                            CURLOPT_URL => $login_url,
                            CURLOPT_POST => 3,
                            CURLOPT_POSTFIELDS => http_build_query(array(
                                'login' => $username,
                                'passwd' => $password,
                                'KMSI' => 1, // Stay signed in
                                'PPFT' => $ppft[1]
                            )),
                            CURLOPT_FOLLOWLOCATION => true
                        ));
                        $auth_result = curl_exec($ch);
                        $auth_url = curl_getinfo($ch)['url'];
                        curl_close($ch);

                        if (strpos($auth_url, $url.'?code') === 0) {
                            return true;
                        }
                    }
                    return false;
                    break;
            }
            $result_url = $result['url'];
            if ($result['http_code'] == 302) $result_url = $result['redirect_url'];

            // Account has not been registered with Bungie
            if (strpos($result_url, '/Register') !== false) return false;

            // Login successful, "bungleatk" should be set
            // Facebook/PSN should return with ?code=
            // Xbox should have ?wa=wsignin1.0
            return strpos($result_url, $url) === 0;
        }
        // Valid Third Party Cookies, re-authenticating Bungie Login
        $ch = curl_init();
        curl_setopt_array($ch, $default_options);
        curl_setopt_array($ch, array(
            CURLOPT_URL => $auth_url,
        ));
        curl_exec($ch);
        curl_close($ch);
        return true;
    }

}