<?php
require_once __DIR__ . '/../src/bootstrap.php';
use Korireed\DgafClan\Services\BungieApi;
$bng = new BungieApi();

$group = $bng->getGroup(490142);
$gId = $group['Response']['detail']['groupId']; //490142
$numClanMembers = $group['Response']['clanMembershipTypes'][0]['memberCount']; //47
$itemsPerPage = 10;
$numPages = intval($numClanMembers/$itemsPerPage);
if(($numClanMembers % $itemsPerPage) > 0) { $numPages++; } // add another page if we didn't get all the members
$adminIds = array();
$adminArray = array();
$memberArray = array();

foreach($bng->getGroupAdmins($gId)['Response']['results'] as $user) {
    array_push($adminIds,$user['membershipId']);
    array_push($adminArray,$user);
}

for($i = 1; $i <= $numPages; $i++) {
    foreach ($bng->getGroupMembers($gId,$i)['Response']['results'] as $user) {
        if(!in_array($user['user']['membershipId'],$adminIds)) {
            array_push($memberArray,$user);
        }
    }
}

function compare_lastname($a, $b) {
    return strnatcmp($a['user']['xboxDisplayName'], $b['user']['xboxDisplayName']);
}
// sort alphabetically by name
usort($memberArray, 'compare_lastname');

for($i = sizeof($adminArray)-1; $i >= 0; $i--) {
    array_unshift($memberArray,$adminArray[$i]);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="The Dauntless Guardian Angels Federation is an Xbox One gaming clan formed to play Destiny. We focus on Raids, PoE, and Weeklies. We are 18+, non-discriminating, and open to all Destiny players!">
    <meta name="keywords" content="Destiny, Xbox One, Gaming, Raids, Prison of Elders, PoE, Crucible, Community, Clan">
    <meta name="author" content="Cory J. Reid">
    <title>Clan Roster - Dauntless Guardian Angels - "Alone we are mighty; together, we are invincible!"</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="roster">
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">D<span class="hidden-xs">auntless </span>G<span class="hidden-xs">uardian </span>A<span class="hidden-xs">ngels </span><span class="visible-xs-inline">F Clan</span></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php" title="Dauntless Home">Home</a></li>
                <li class="active"><a href="roster.php" title="Dauntless Roster">Roster</a></li>
                <li role="separator" class="divider">&nbsp;</li>
                <li>
                    <p class="navbar-btn text-center">
                        <a href="https://www.bungie.net/en/Clan/Post/490142/134613359/0/0" class="btn btn-primary">Join Us!</a>
                    </p>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<section class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Clan Roster</h1>
            <p>All members are asked to set Dauntless as their active clan. Those that have are listed below. Please find us in-game and play with us!</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed" id="roster">
                    <thead>
                        <th>Avatar</th>
                        <th>Gamertag</th>
                        <th>Clan Rank</th>
                        <th>Player Stats</th>
                    </thead>
                    <?php

                    foreach($memberArray as $user) {
                        $memberType = 'Member';
                        $avatarUrl = 'https://www.bungie.net' . $user['user']['profilePicturePath'];
                        if(strpos($user['user']['profilePicturePath'],"http") === 0) { $avatarUrl = $user['user']['profilePicturePath']; }
                        $row = "<tr";
                        if(in_array($user['user']['membershipId'],$adminIds)) {
                            $memberType = 'Admin';
                            $row .= " class=\"";
                            if($user['isOriginalFounder']) {
                                $memberType = 'Founder';
                                $row .= "info";
                            } else {
                                $row .= "active";
                            }
                            $row .= "\"";
                        }
                        $row .= ">";
                        $row .= "<td><img src=\"" . $avatarUrl . "\" height=\"50px\" width=\"50px\" alt=\"" . $user['user']['xboxDisplayName'] . "\" /></td>"; //avatar
                        $row .= "<td>" . $user['user']['xboxDisplayName'] . "</td>"; //gamertag
                        $row .= "<td>" . $memberType . "</td>"; //rank
                        $row .= "<td><a href=\"http://destinytracker.com/destiny/player/xbox/" . rawurlencode($user['user']['xboxDisplayName']) . "\" title=\"\">Statistics</a></td>"; //stats link
                        $row .= "</tr>";
                        echo $row;
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
</section>

<footer class="footer">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12">
                <h3>Summary</h3>
                <p>Our clan tag is <strong>[DGAF]</strong> and stands for Dauntless Guardian Angels Federation. We curse, we scream, and we laugh. Having fun while while being Dauntless is what we do. Our focus is primarily on Raids, PoEs, and Weeklies. We also enjoy dabbling in Crucible.</p>
            </div>
            <div class="col-xs-12"><p>&nbsp;</p><p class="totop"><a href="#roster" title="To Top"><span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span></a></p></div>
            <div class="col-xs-12">
                <ul class="nav nav-justified">
                    <li><a href="https://www.bungie.net/en/Clan/490142" title="DGAF on Bungie.net">Forums</a></li>
                    <li><a href="http://destinytracker.com/clans/490142/dauntless-guardian-angels/raid" title="DGAF Raid Statistics">Raid Stats</a></li>
                    <li><a href="http://destinytracker.com/clans/490142/dauntless-guardian-angels/allPvP" title="DGAF PvP Statistics">PvP Stats</a></li>
                    <li><a href="https://www.youtube.com/channel/UClrw0roSvRDFmJpkrKoxRWA" title="DGAF on YouTube">YouTube</a></li>
                    <li><a href="https://www.facebook.com/groups/dgafclan" title="DGAF on Facebook">Facebook</a></li>
                    <li><a href="https://twitter.com/dgafclan" title="DGAF on Twitter">Twitter</a></li>
                </ul>
            </div>
            <div class="col-xs-12"><p><small>All content copyright &copy; 2015 <a href="http://www.coryjreid.com/" title="Cory J. Reid Designs">Cory J. Reid</a>. All rights reserved.</small></p></div>
        </div>
    </div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-2.1.4.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>