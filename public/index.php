<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="The Dauntless Guardian Angels Federation is an Xbox One gaming clan formed to play Destiny. We focus on Raids, PoE, and Weeklies. We are 18+, non-discriminating, and open to all Destiny players!">
        <meta name="keywords" content="Destiny, Xbox One, Gaming, Raids, Prison of Elders, PoE, Crucible, Community, Clan">
        <meta name="author" content="Cory J. Reid">
        <title>Dauntless Guardian Angels - "Alone we are mighty; together, we are invincible!"</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="home">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">D<span class="hidden-xs">auntless </span>G<span class="hidden-xs">uardian </span>A<span class="hidden-xs">ngels </span><span class="visible-xs-inline">F Clan</span></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.php" title="Dauntless Home">Home</a></li>
                        <li><a href="roster.php" title="Dauntless Roster">Roster</a></li>
                        <li role="separator" class="divider">&nbsp;</li>
                        <li>
                            <p class="navbar-btn text-center">
                                <a href="https://www.bungie.net/en/Clan/Post/490142/134613359/0/0" class="btn btn-primary">Join Us!</a>
                            </p>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <section id="slide1" class="slide">
            <div class="container-fluid">
                <div class="v-center">
                    <div class="v-content">
                        <h1>Dauntless Guardian Angels</h1>
                        <p class="lead">Alone we are mighty; together, we are invincible."</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="slide2" class="slide between">
            <div class="container-fluid">
                <div class="row text-center">
                    <div class="col-xs-12"><h2>We Do</h2><p><!-- spacer p -->&nbsp;</p></div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <p class="lead">Raids</p>
                        <p class="hidden-xs">Killed them all.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <p class="lead">Prison of Elders</p>
                        <p class="hidden-xs">Skolas is our bitch.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <p class="lead">Story &amp; Leveling</p>
                        <p class="hidden-xs">We play our alts.</p>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <p class="lead">Nightfalls</p>
                        <p class="hidden-xs">Done in 10 minutes.</p>
                    </div>
                    <div class="col-xs-12"><p><!-- spacer p -->&nbsp;</p><p class="lead"><b>We [kick ass at] Destiny</b></p></div>
                </div>
            </div>
        </section>
        <section id="slide3" class="slide">
            <div class="container-fluid">
                <div class="v-center">
                    <div class="v-content">
                        <h2>Fearless</h2>
                        <p>Dauntless believes in charging into the unknown without hesitation. We do not know fear!</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="slide4" class="slide between">
            <div class="container-fluid">
                <div class="row text-center">
                    <div class="col-xs-12"><h2><span class="hidden-xs">Membership </span>Requirements</h2><p><!-- spacer p -->&nbsp;</p></div>
                    <div class="col-xs-12 col-sm-4 col-md-4"><p class="lead">Xbox One</p><p class="hidden-xs">XB1 > PS4.</p></div>
                    <div class="col-xs-12 col-sm-4 col-md-4"><p class="lead">Ages 18+<p class="hidden-xs">Like a fine wine.</p></p></div>
                    <div class="col-xs-12 col-sm-4 col-md-4"><p class="lead">Headset Mic</p><p class="hidden-xs">No mic. No raid.</p></div>
                    <div class="col-xs-12"><p><!-- spacer p -->&nbsp;</p><p><a href="https://www.bungie.net/en/Clan/Post/490142/132318753/0/0" class="btn btn-primary btn-lg" role="button">Learn More</a></p></div>
                </div>
            </div>
        </section>
        <section id="slide5" class="slide">
            <div class="container-fluid">
                <div class="v-center">
                    <div class="v-content">
                        <h2>Skilled</h2>
                        <p>Dauntless and its members are skilled, calculated in action, and occasionally mouthy - with good intentions.</p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="footer">
            <div class="container">
                <div class="row text-center">
                    <div class="col-xs-12">
                        <h3>Summary</h3>
                        <p>Our clan tag is <strong>[DGAF]</strong> and stands for Dauntless Guardian Angels Federation. We curse, we scream, and we laugh. Having fun while while being Dauntless is what we do. Our focus is primarily on Raids, PoEs, and Weeklies. We also enjoy dabbling in Crucible.</p>
                    </div>
                    <div class="col-xs-12"><p>&nbsp;</p><p class="totop"><a href="#home" title="To Top"><span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span></a></p></div>
                    <div class="col-xs-12">
                        <ul class="nav nav-justified">
                            <li><a href="https://www.bungie.net/en/Clan/490142" title="DGAF on Bungie.net">Forums</a></li>
                            <li><a href="http://destinytracker.com/clans/490142/dauntless-guardian-angels/raid" title="DGAF Raid Statistics">Raid Stats</a></li>
                            <li><a href="http://destinytracker.com/clans/490142/dauntless-guardian-angels/allPvP" title="DGAF PvP Statistics">PvP Stats</a></li>
                            <li><a href="https://www.youtube.com/channel/UClrw0roSvRDFmJpkrKoxRWA" title="DGAF on YouTube">YouTube</a></li>
                            <li><a href="https://www.facebook.com/groups/dgafclan" title="DGAF on Facebook">Facebook</a></li>
                            <li><a href="https://twitter.com/dgafclan" title="DGAF on Twitter">Twitter</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12"><p><small>All content copyright &copy; 2015 <a href="http://www.coryjreid.com/" title="Cory J. Reid Designs">Cory J. Reid</a>. All rights reserved.</small></p></div>
                </div>
            </div>
        </footer>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-2.1.4.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>