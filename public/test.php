<?php
require_once __DIR__ . '/../src/bootstrap.php';

use Korireed\DgafClan\Services\BungieApi;

$bng = new BungieApi();
//https://www.bungie.net/Platform/Group/<group id here>/Members/?lc=en&fmt=true&currentPage=1&platformType=2
//https://www.bungie.net/Platform/Destiny/1/Account/4611686018433753722/Character/2305843009216390517/Complete/'

$group = $bng->getGroup(490142);
$gId = $group['Response']['detail']['groupId']; //490142
$numClanMembers = $group['Response']['clanMembershipTypes'][0]['memberCount']; //47
$itemsPerPage = 10;
$numPages = intval($numClanMembers/$itemsPerPage);
if(($numClanMembers % $itemsPerPage) > 0) { $numPages++; } // add another page if we didn't get all the members
$adminArray = array();
$memberArray = array();

foreach($bng->getGroupAdmins($gId)['Response']['results'] as $user) {
    array_push($adminArray,$user['membershipId']);
}

for($i = 1; $i <= $numPages; $i++) {
    foreach ($bng->getGroupMembers($gId,$i)['Response']['results'] as $user) {
        array_push($memberArray,$user);
    }
}

//print_r($memberArray[0]['user']['xboxDisplayName']);
print_r($memberArray);